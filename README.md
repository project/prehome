CONTENTS OF THIS FILE
---------------------

* Introduction
* Installation
* Requirements
* Usage
* Templating
* Maintainers


INTRODUCTION
------------

This module allows to display a modal on the first visit, using a new dedicated
entity type named 'prehome'.


INSTALLATION
------------

Install the Prehome module as you would normally install a contributed Drupal
module (see https://www.drupal.org/node/1897420 for more information).


REQUIREMENTS
------------

No requirements.


USAGE
-----

1. Add `prehome_display_count` to the allowed cookies, in your GDPR module (if
   any).
2. Set prehome fields, form & display settings to fit your requirements in
   Structure > Prehome (`admin/structure/prehome/settings`).

   Out of the box it contains a single WYSIWYG field, but you can easily add some
   more to fit your requirements (image, plain text, link, etc.).
3. Add at least one prehome in Content > Prehome (`admin/structure/prehome`).
4. Configure prehome display settings in Configuration > Prehome
   (`admin/config/prehome/settings`).


TEMPLATING
----------

You can use usual templating process to render the prehome, by overriding
the default template with :
```
* prehome--[entity_id]--[view_mode].html.twig
* prehome--[view_mode].html.twig
x prehome.html.twig
```

And by implementing `hook_preprocess_prehome()` if needed.


MAINTAINERS
-----------

* Nicolas Nucci (NicociN) - https://www.drupal.org/u/nicocin

Supporting organizations:

* Ecedi - https://www.drupal.org/ecedi
