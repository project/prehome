<?php

namespace Drupal\prehome\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Prehome entities.
 *
 * @ingroup prehome
 */
interface PrehomeInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Prehome name.
   *
   * @return string
   *   Name of the Prehome.
   */
  public function getName();

  /**
   * Sets the Prehome name.
   *
   * @param string $name
   *   The Prehome name.
   *
   * @return \Drupal\prehome\Entity\PrehomeInterface
   *   The called Prehome entity.
   */
  public function setName($name);

  /**
   * Gets the Prehome creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Prehome.
   */
  public function getCreatedTime();

  /**
   * Sets the Prehome creation timestamp.
   *
   * @param int $timestamp
   *   The Prehome creation timestamp.
   *
   * @return \Drupal\prehome\Entity\PrehomeInterface
   *   The called Prehome entity.
   */
  public function setCreatedTime($timestamp);

}
