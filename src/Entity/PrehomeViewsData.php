<?php

namespace Drupal\prehome\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Prehome entities.
 */
class PrehomeViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
