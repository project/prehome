<?php

namespace Drupal\prehome;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for prehome.
 */
class PrehomeTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
