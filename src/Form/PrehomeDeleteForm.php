<?php

namespace Drupal\prehome\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Prehome entities.
 *
 * @ingroup prehome
 */
class PrehomeDeleteForm extends ContentEntityDeleteForm {


}
