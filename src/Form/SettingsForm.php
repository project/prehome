<?php

namespace Drupal\prehome\Form;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 *
 * Form to configure prehome settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The entity type manager definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository definition.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepository
   */
  protected $entityDisplayRepository;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->entityDisplayRepository = $container->get('entity_display.repository');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'prehome.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('prehome.settings');

    $form['show'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display prehome'),
      '#default_value' => $config->get('show'),
    ];

    $published_prehomes = $this->entityTypeManager->getStorage('prehome')->loadByProperties(['status' => TRUE]);
    $prehome_options = [];
    foreach ($published_prehomes as $key => $published_prehome) {
      $prehome_options[$published_prehome->id()] = $published_prehome->label();
    }
    $form['prehome'] = [
      '#type' => 'select',
      '#title' => $this->t('Prehome'),
      '#options' => $prehome_options,
      '#size' => 1,
      '#default_value' => $config->get('prehome'),
      '#description' => $this->t('Manage Prehomes <a href="/admin/structure/prehome">here</a>.'),
    ];
    if (empty($prehome_options)) {
      $form['prehome']['#description'] = $this->t('Please add at least one prehome <a href="/admin/structure/prehome">here</a>.');
    }

    // View modes.
    $form['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('View mode'),
      '#options' => $this->entityDisplayRepository->getViewModeOptionsByBundle('prehome', 'prehome'),
      '#size' => 1,
      '#default_value' => $config->get('view_mode') ?? 'default',
      '#description' => $this->t('To add more view modes, go to <a href="/admin/structure/display-modes/view/add/prehome">this page</a>.'),
    ];

    $form['nb_show'] = [
      '#type' => 'number',
      '#min' => 0,
      '#title' => $this->t('Number of displays'),
      '#description' => $this->t('Set the number of times you want to show the Prehome for a user. Set to 0 for no limit.'),
      '#default_value' => $config->get('nb_show') ?? 0,
    ];

    // Display pages.
    $form['pages'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#default_value' => $config->get('pages'),
      '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. An example path is %node-wildcard for every user page. %front is the front page.", [
        '%node-wildcard' => '/node/*',
        '%front' => '<front>',
      ]),
    ];

    $form['negate_urls'] = [
      '#type' => 'radios',
      '#options' => [
        0 => $this->t('Show for the listed pages'),
        1 => $this->t('Hide for the listed pages'),
      ],
      '#default_value' => $config->get('negate_urls'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Modal title'),
      '#description' => $this->t('The modal title is visually hidden.'),
      '#default_value' => $config->get('title'),
    ];

    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Additionnal classes'),
      '#description' => $this->t('Classes separated by a space. They will be added to the modal wrapper.'),
      '#default_value' => $config->get('classes'),
    ];

    $form['dont_close_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('"Don\'t close" class'),
      '#description' => $this->t("Elements with this class won't close the Prehome when clicked (like cookie banner)."),
      '#default_value' => $config->get('dont_close_class'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('show') && empty($form_state->getValue('prehome'))) {
      $form_state->setErrorByName('prehome', $this->t('Please select a prehome before displaying it.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('prehome.settings');
    $config->set('show', $form_state->getValue('show'));
    $config->set('prehome', $form_state->getValue('prehome'));
    $config->set('view_mode', $form_state->getValue('view_mode'));
    $config->set('title', $form_state->getValue('title'));
    $config->set('nb_show', $form_state->getValue('nb_show'));
    $config->set('pages', $form_state->getValue('pages'));
    $config->set('negate_urls', $form_state->getValue('negate_urls'));
    $config->set('dont_close_class', $form_state->getValue('dont_close_class'));
    // Sanitize classes.
    $classes = explode(' ', $form_state->getValue('classes'));
    $classes = array_filter($classes);
    $classes_sanitized = [];
    foreach ($classes as $class) {
      $classes_sanitized[] = Html::cleanCssIdentifier($class);
    }
    $classes_as_string = implode(' ', $classes_sanitized);
    $config->set('classes', $classes_as_string);

    $config->save();
  }

}
