<?php

namespace Drupal\prehome;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Prehome entity.
 *
 * @see \Drupal\prehome\Entity\Prehome.
 */
class PrehomeAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\prehome\Entity\PrehomeInterface $entity */

    switch ($operation) {

      case 'view':

        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished prehome entities');
        }

        return AccessResult::allowedIfHasPermission($account, 'view published prehome entities');

      case 'update':

        return AccessResult::allowedIfHasPermission($account, 'edit prehome entities');

      case 'delete':

        return AccessResult::allowedIfHasPermission($account, 'delete prehome entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add prehome entities');
  }

}
