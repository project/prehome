/**
 * @file
 * Provides modal feature.
 *
 * The extra line between the end of the @file docblock
 * and the file-closure is important.
 */

'use strict';

(function (Drupal, drupalSettings, cookies) {
  /**
   * Displays the prehome in a modal.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   *   Opens the modal.
   */
  Drupal.behaviors.prehomeModal = {
    /**
     * Attach behaviors.
     *
     * @param context
     *   This is the dom element of context.
     */
    attach: function (context) {
      once('prehome-modal', '#prehome-modal', context).forEach(function (element) {
        var displayCount = parseInt(cookies.get('prehome_display_count'));
        displayCount = !Number.isInteger(displayCount) ? 0 : displayCount;
        var nbTimesToShow = parseInt(drupalSettings.times_to_show) ? parseInt(drupalSettings.times_to_show) : 0;

        if (displayCount < nbTimesToShow || nbTimesToShow === 0) {
          // Display modal.
          Drupal.dialog(element, {
            title: drupalSettings.title,
            classes: {
              'ui-dialog': drupalSettings.classes,
            },
            draggable: false,
            open: function (event, ui) {
              this.parentNode.focus();

              // Close modal on overlay click.
              document.addEventListener('click', function (event) {
                if (event.target.closest('.prehome .ui-widget-content') === null) {
                  // Prevent modal close on "protected" element.
                  var dontCloseClass = drupalSettings.dont_close_class;
                  if (dontCloseClass !== '' && event.target.closest('.' + dontCloseClass) !== null) {
                    return;
                  }
                  // Close modal.
                  var closeButton = document.getElementsByClassName('ui-dialog-titlebar-close');
                  if (closeButton.length) {
                    closeButton[0].click();
                  }
                }
              })

              // Increment display count.
              cookies.set('prehome_display_count', displayCount + 1, {path: '/', secure: true});
            },
          }).showModal();
        }
      });
    }
  };

}(Drupal, drupalSettings, window.Cookies));
